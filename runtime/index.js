function getEnvironment () {
    switch (process.env.RUNTIME_ENV) {
    case 'prod':
        return require('./production')
    case 'qa':
        return require('./qa')
    case 'local':
        return require('./local')
    default:
        return require('./production')
    }
}

module.exports = getEnvironment()
