const prod = {}

prod.todoist = {
    host: 'https://api.todoist.com/rest/v1',
    token: process.env.TODOIST_TOKEN,
    shortTimeout: 400,
    mediumTimeout: 1500,
    largeTimeout: 5000,
    extraLargeTimeout: 30000
}

module.exports = prod
