const schemas = {
    getAllUpdatedTasks: {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "array",
        "items": [
          {
            "type": "object",
            "properties": {
              "id": {
                "type": "integer"
              },
              "assigner": {
                "type": "integer"
              },
              "project_id": {
                "type": "integer"
              },
              "section_id": {
                "type": "integer"
              },
              "order": {
                "type": "integer"
              },
              "content": {
                "type": "string"
              },
              "completed": {
                "type": "boolean"
              },
              "label_ids": {
                "type": "array",
                "items": {}
              },
              "priority": {
                "type": "integer"
              },
              "comment_count": {
                "type": "integer"
              },
              "creator": {
                "type": "integer"
              },
              "created": {
                "type": "string"
              },
              "url": {
                "type": "string"
              }
            },
            "required": [
              "id",
              "assigner",
              "project_id",
              "section_id",
              "order",
              "content",
              "completed",
              "label_ids",
              "priority",
              "comment_count",
              "creator",
              "created",
              "url"
            ]
          }
        ]
      },
      createNewTask: {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "properties": {
          "id": {
            "type": "integer"
          },
          "assigner": {
            "type": "integer"
          },
          "project_id": {
            "type": "integer"
          },
          "section_id": {
            "type": "integer"
          },
          "order": {
            "type": "integer"
          },
          "content": {
            "type": "string"
          },
          "completed": {
            "type": "boolean"
          },
          "label_ids": {
            "type": "array",
            "items": {}
          },
          "priority": {
            "type": "integer"
          },
          "comment_count": {
            "type": "integer"
          },
          "creator": {
            "type": "integer"
          },
          "created": {
            "type": "string"
          },
          "url": {
            "type": "string"
          }
        },
        "required": [
          "id",
          "assigner",
          "project_id",
          "section_id",
          "order",
          "content",
          "completed",
          "label_ids",
          "priority",
          "comment_count",
          "creator",
          "created",
          "url"
        ]
      },
      getTask: {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "properties": {
          "id": {
            "type": "integer"
          },
          "assigner": {
            "type": "integer"
          },
          "project_id": {
            "type": "integer"
          },
          "section_id": {
            "type": "integer"
          },
          "order": {
            "type": "integer"
          },
          "content": {
            "type": "string"
          },
          "completed": {
            "type": "boolean"
          },
          "label_ids": {
            "type": "array",
            "items": {}
          },
          "priority": {
            "type": "integer"
          },
          "comment_count": {
            "type": "integer"
          },
          "creator": {
            "type": "integer"
          },
          "created": {
            "type": "string"
          },
          "url": {
            "type": "string"
          }
        },
        "required": [
          "id",
          "assigner",
          "project_id",
          "section_id",
          "order",
          "content",
          "completed",
          "label_ids",
          "priority",
          "comment_count",
          "creator",
          "created",
          "url"
        ]
      }
}

module.exports = schemas
