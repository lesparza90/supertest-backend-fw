const schemas = {
    getAllProjects: {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "array",
        "items": [
          {
            "type": "object",
            "properties": {
              "id": {
                "type": "integer"
              },
              "order": {
                "type": "integer"
              },
              "color": {
                "type": "integer"
              },
              "name": {
                "type": "string"
              },
              "comment_count": {
                "type": "integer"
              },
              "shared": {
                "type": "boolean"
              },
              "favorite": {
                "type": "boolean"
              },
              "sync_id": {
                "type": "integer"
              }
            },
            "required": [
              "id",
              "color",
              "name",
              "comment_count",
              "shared",
              "favorite",
              "sync_id"
            ]
          }
        ]
    },
    createNewProject: {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "properties": {
          "id": {
            "type": "integer"
          },
          "order": {
            "type": "integer"
          },
          "color": {
            "type": "integer"
          },
          "name": {
            "type": "string"
          },
          "comment_count": {
            "type": "integer"
          },
          "shared": {
            "type": "boolean"
          },
          "favorite": {
            "type": "boolean"
          },
          "sync_id": {
            "type": "integer"
          }
        },
        "required": [
          "id",
          "order",
          "color",
          "name",
          "comment_count",
          "shared",
          "favorite",
          "sync_id"
        ]
    },
    getProject: {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "properties": {
          "id": {
            "type": "integer"
          },
          "order": {
            "type": "integer"
          },
          "color": {
            "type": "integer"
          },
          "name": {
            "type": "string"
          },
          "comment_count": {
            "type": "integer"
          },
          "shared": {
            "type": "boolean"
          },
          "favorite": {
            "type": "boolean"
          },
          "sync_id": {
            "type": "integer"
          }
        },
        "required": [
          "id",
          "order",
          "color",
          "name",
          "comment_count",
          "shared",
          "favorite",
          "sync_id"
        ]
    }
}

module.exports = schemas
