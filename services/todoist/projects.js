const superagent = require('superagent')
require('superagent-retry-delay')(superagent)
const supertest = require('supertest')
const runtime = require('../../runtime')
const log = require('../../utils/log')

// environment factors
const timeout = runtime.todoist.mediumTimeout
// host settings
const todoistHost = runtime.todoist.host
const todoisToken = runtime.todoist.token

const request = supertest(todoistHost)

const requests = {
    getAllProjects: (test) => {
        return request.get('/projects')
            .auth(todoisToken, { type: 'bearer' })
            .timeout(timeout)
            .retry(3, 5000, [401, 404])
            .then((res) => {
                log.fullLogToReport(test, res, `GET ${todoistHost}/projects`)
                return res
            })
    },
    getProject: (test, id) => {
        return request.get(`/projects/${id}`)
            .auth(todoisToken, { type: 'bearer' })
            .timeout(timeout)
            .retry(3, 5000, [401, 404])
            .then((res) => {
                log.fullLogToReport(test, res, `GET ${todoistHost}/projects/${id}`)
                return res
            })
    },
    createNewProject: (test, args) => {
        return request.post('/projects')
            .set('Content-Type', 'application/json')
            .auth(todoisToken, { type: 'bearer' })
            .send(args)
            .timeout(timeout)
            .retry(3, 5000, [401])
            .then((res) => {
                log.fullLogToReport(test, res, `POST ${todoistHost}/projects`, args)
                return res
            })
    },
    updateProject: (test, id, args) => {
        return request.post(`/projects/${id}`)
            .set('Content-Type', 'application/json')
            .auth(todoisToken, { type: 'bearer' })
            .send(args)
            .timeout(timeout)
            .retry(3, 5000, [401, 404])
            .then((res) => {
                log.fullLogToReport(test, res, `POST ${todoistHost}/projects/${id}`, args)
                return res
            })
    },
    deleteProject: (test, id) => {
        return request.delete(`/projects/${id}`)
            .auth(todoisToken, { type: 'bearer' })
            .timeout(timeout)
            .retry(3, 5000, [401, 404])
            .then((res) => {
                log.fullLogToReport(test, res, `DELETE ${todoistHost}/projects/${id}`)
                return res
            })
    }
}

module.exports = requests
