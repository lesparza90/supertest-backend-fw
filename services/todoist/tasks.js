const superagent = require('superagent')
require('superagent-retry-delay')(superagent)
const supertest = require('supertest')
const runtime = require('../../runtime')
const log = require('../../utils/log')

// environment factors
const timeout = runtime.todoist.mediumTimeout
// host settings
const todoistHost = runtime.todoist.host
const todoisToken = runtime.todoist.token

const request = supertest(todoistHost)

const requests = {
    getAllTasks: (test) => {
        return request.get('/tasks')
            .auth(todoisToken, { type: 'bearer' })
            .timeout(timeout)
            .retry(3, 5000, [401, 404])
            .then((res) => {
                log.fullLogToReport(test, res, `GET ${todoistHost}/tasks`)
                return res
            })
    },
    getTasks: (test, id) => {
        return request.get(`/tasks/${id}`)
            .auth(todoisToken, { type: 'bearer' })
            .timeout(timeout)
            .retry(3, 5000, [401, 404])
            .then((res) => {
                log.fullLogToReport(test, res, `GET ${todoistHost}/tasks/${id}`)
                return res
            })
    },
    createNewTasks: (test, args) => {
        return request.post('/tasks')
            .set('Content-Type', 'application/json')
            .auth(todoisToken, { type: 'bearer' })
            .send(args)
            .timeout(timeout)
            .retry(3, 5000, [401, 404])
            .then((res) => {
                log.fullLogToReport(test, res, `POST ${todoistHost}/tasks`, args)
                return res
            })
    },
    updateTasks: (test, id, args) => {
        return request.post(`/tasks/${id}`)
            .set('Content-Type', 'application/json')
            .auth(todoisToken, { type: 'bearer' })
            .send(args)
            .timeout(timeout)
            .retry(3, 5000, [401, 404])
            .then((res) => {
                log.fullLogToReport(test, res, `POST ${todoistHost}/tasks/${id}`, args)
                return res
            })
    },
    deleteTasks: (test, id) => {
        return request.delete(`/tasks/${id}`)
            .auth(todoisToken, { type: 'bearer' })
            .timeout(timeout)
            .retry(3, 5000, [401, 404])
            .then((res) => {
                log.fullLogToReport(test, res, `DELETE ${todoistHost}/tasks/${id}`)
                return res
            })
    },
    closeTasks: (test, id) => {
        return request.post(`/tasks/${id}/close`)
            .auth(todoisToken, { type: 'bearer' })
            .timeout(timeout)
            .retry(3, 5000, [401, 404])
            .then((res) => {
                log.fullLogToReport(test, res, `POST ${todoistHost}/tasks/${id}/close`)
                return res
            })
    },
    reopenTasks: (test, id) => {
        return request.post(`/tasks/${id}/reopen`)
            .auth(todoisToken, { type: 'bearer' })
            .timeout(timeout)
            .retry(3, 5000, [401, 404])
            .then((res) => {
                log.fullLogToReport(test, res, `POST ${todoistHost}/tasks/${id}/reopen`)
                return res
            })
    }
}

module.exports = requests
