const chai = require('chai')
const dirtyChai = require('dirty-chai')
const expect = chai.expect
const tasks = require('../../services/todoist/tasks')
const schemas = require('../../schemas/todoist/tasks.schema')
const baseTest = require('../baseTest')
const faker = require('faker')
chai.use(dirtyChai)

describe('Tasks E2E Testing', function () {
    let tasksID = null
    const taskContent = faker.lorem.sentence()
    const updatedTaskContent = faker.lorem.sentence()
    const taskPriority = faker.random.number({ min: 1, max: 4 })
    const taskDueDate = faker.date.future()

    it('GET /tasks', async function () {
        const res = await tasks.getAllTasks(this)
        expect(res.status).to.be.eq(200)
        expect(res.body).to.not.be.empty()
        const schemaResult = await baseTest.validateSchema(this, res.body, schemas.getAllUpdatedTasks)
        expect(schemaResult.valid, 'schema validation failed').to.be.true()
    })

    it('POST /tasks', async function () {
        const args = { content: taskContent }
        const res = await tasks.createNewTasks(this, args)
        expect(res.status).to.be.eq(200)
        expect(res.body).to.not.be.empty()
        const schemaResult = await baseTest.validateSchema(this, res.body, schemas.createNewTask)
        expect(schemaResult.valid, 'schema validation failed').to.be.true()
        tasksID = res.body.id
    })

    it('POST /tasks/:id', async function () {
        const args = {
            content: updatedTaskContent,
            priority: taskPriority,
            due_datetime: taskDueDate
        }
        const res = await tasks.updateTasks(this, tasksID, args)
        expect(res.status).to.be.eq(204)
        expect(res.body).to.be.empty()
    })

    it('GET /tasks/:id', async function () {
        const res = await tasks.getTasks(this, tasksID)
        expect(res.status).to.be.eq(200)
        expect(res.body).to.not.be.empty()
        expect(res.body.content).to.be.eq(updatedTaskContent)
        expect(res.body.priority).to.be.eq(taskPriority)
        expect(res.body.due.datetime).to.be.eq(taskDueDate.toISOString().split('.')[0] + 'Z')
        const schemaResult = await baseTest.validateSchema(this, res.body, schemas.getTask)
        expect(schemaResult.valid, 'schema validation failed').to.be.true()
    })

    it('POST /tasks/:id/close', async function () {
        const res = await tasks.closeTasks(this, tasksID)
        expect(res.status).to.be.eq(204)
        expect(res.body).to.be.empty()
    })

    it('GET /tasks/:id', async function () {
        const res = await tasks.getTasks(this, tasksID)
        expect(res.status).to.be.eq(404)
        expect(res.body).to.be.empty()
    })

    it('POST /tasks/:id/reopen', async function () {
        const res = await tasks.reopenTasks(this, tasksID)
        expect(res.status).to.be.eq(204)
        expect(res.body).to.be.empty()
    })

    it('GET /tasks/:id', async function () {
        const res = await tasks.getTasks(this, tasksID)
        expect(res.status).to.be.eq(200)
        expect(res.body).to.not.be.empty()
        expect(res.body.content).to.be.eq(updatedTaskContent)
        expect(res.body.priority).to.be.eq(taskPriority)
        expect(res.body.due.datetime).to.be.eq(taskDueDate.toISOString().split('.')[0] + 'Z')
        const schemaResult = await baseTest.validateSchema(this, res.body, schemas.getTask)
        expect(schemaResult.valid, 'schema validation failed').to.be.true()
    })

    it('DELETE /tasks/:id', async function () {
        const res = await tasks.deleteTasks(this, tasksID)
        expect(res.status).to.be.eq(204)
        expect(res.body).to.be.empty()
    })

    it('GET /tasks/:id', async function () {
        const res = await tasks.getTasks(this, tasksID)
        expect(res.status).to.be.eq(404)
        expect(res.body).to.be.empty()
    })
})
