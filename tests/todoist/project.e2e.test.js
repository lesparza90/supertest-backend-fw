const chai = require('chai')
const dirtyChai = require('dirty-chai')
const expect = chai.expect
const projects = require('../../services/todoist/projects')
const schemas = require('../../schemas/todoist/projects.schema')
const baseTest = require('../baseTest')
const faker = require('faker')
chai.use(dirtyChai)

describe('Projects E2E Testing', function () {
    let projectID = null
    const projectName = faker.lorem.words()
    const colorProject = faker.random.number({ min: 30, max: 49 })
    const favoriteProject = faker.random.boolean()

    it('GET /projects', async function () {
        const res = await projects.getAllProjects(this)
        expect(res.status).to.be.eq(200)
        expect(res.body).to.not.be.empty()
        const schemaResult = await baseTest.validateSchema(this, res.body, schemas.getAllProjects)
        expect(schemaResult.valid, 'Schema validation failed').to.be.true()
    })

    it('POST /projects', async function () {
        const args = { name: 'Movies to not watch' }
        const res = await projects.createNewProject(this, args)
        expect(res.status).to.be.eq(200)
        expect(res.body).to.not.be.empty()
        const schemaResult = await baseTest.validateSchema(this, res.body, schemas.createNewProject)
        expect(schemaResult.valid, 'Schema validation failed').to.be.true()
        projectID = res.body.id
    })

    it('POST /projects/:id', async function () {
        const args = {
            name: projectName,
            color: colorProject,
            favorite: favoriteProject
        }
        const res = await projects.updateProject(this, projectID, args)
        expect(res.status).to.be.eq(204)
        expect(res.body).to.be.empty()
    })

    it('GET /projects/:id', async function () {
        const res = await projects.getProject(this, projectID)
        expect(res.status).to.be.eq(200)
        expect(res.body).to.not.be.empty()
        expect(res.body.name).to.be.eq(projectName)
        expect(res.body.color).to.be.eq(colorProject)
        expect(res.body.favorite).to.be.eq(favoriteProject)
        const schemaResult = await baseTest.validateSchema(this, res.body, schemas.getProject)
        expect(schemaResult.valid, 'Schema validation failed').to.be.true()
    })

    it('DELETE /projects/:id', async function () {
        const res = await projects.deleteProject(this, projectID)
        expect(res.status).to.be.eq(204)
        expect(res.body).to.be.empty()
    })

    it('GET /projects/:id', async function () {
        const res = await projects.getProject(this, projectID)
        expect(res.status).to.be.eq(404)
        expect(res.body).to.be.empty()
    })
})
