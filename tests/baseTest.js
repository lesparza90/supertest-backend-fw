const tv4 = require('tv4')
const addContext = require('mochawesome/addContext')

const baseTests = {
    validateSchema: async (test, body, schema) => {
        const result = await tv4.validateMultiple(body, schema)
        if (!result.valid) {
            result.errors.forEach(error => {
                console.log(error.message)
                addContext(test,
                    {
                        title: 'Schema Validation Error',
                        value: { message: error.message, dataPath: error.dataPath, params: error.params }
                    })
            })
        }
        return result
    }
}

module.exports = baseTests
