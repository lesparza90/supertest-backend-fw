# backend-testing-framework
An NodeJS API Testing Framework

### Features
  - Check status codes and body response 
  - Validate your body response structure using JSON Schemas
  - Verify your request response time
  - Performe E2E Testing
  - Report your test results in a fancy way

### Tech
This API Testing Framework uses a number of open source projects to work properly:
* [node.js](https://nodejs.org/) - an asynchronous event-driven JavaScript runtime
* [Mocha](https://mochajs.org/) - a JavaScript test framework
* [Chai](https://www.chaijs.com/) - a BDD/TDD assertion library for node
* [faker.js](https://www.npmjs.com/package/faker) - a generator of massive amounts of fake data
* [superagent](https://www.npmjs.com/package/superagent) - a progressive client-side HTTP request library
* [supertest](https://www.npmjs.com/package/supertest) - a HTTP assertions made easy via superagent
* [tv4](https://www.npmjs.com/package/tv4) - a validator for v4 JSON Schema

### Installation and Execution
Requires [Node.js](https://nodejs.org/) v8+ to run.

Clone this repository and install the dependencies

```sh
$ git clone <repo>
$ npm install
```

Set up your auth token as an environment variable
```sh
$ export TODOIST_TOKEN=$token
```

Run your Test and get a html report
```sh
$ npm run test-html-report
```

Run your Test without creating a html report
```sh
$ npm test
```

**Notes:**
- ToDo >> The capability to run 'remote' creates test logs in AWS S3 and outputs to slack.
- RUNTIME_ENV identifies the environment the tests will be run against (i.e. qa or production), production is taken as default environment in case RUNTIME_ENV variable has not been specified.
- e.g. To run all tests in for auth_builder in 'local' mode -- you would use the following command:

### How to create a new test in this framework
1. In case the API you want to test has not been added, include your new microservice/API host to each environmental file in the runtime folder (i.e. qa.js, production.js etc.), Additionally, add the timeout values that apply to the new API and the auth token or credentials.

```javascript
prod.todoist = {
    host: 'https://api.todoist.com/rest/v1',
    token: process.env.TODOIST_TOKEN,
    shortTimeout: 400,
    mediumTimeout: 1500,
    largeTimeout: 5000,
    extraLargeTimeout: 30000
}
```

2. Into the **service** folder create a new folder for your new microservice/API, then create a new JS file (i.e. projects.js) and then difine your endpoints in this file.

```javascript
// get your environment factors
const timeout = runtime.todoist.mediumTimeout
// get your host settings such as host a auth token
const todoistHost = runtime.todoist.host
const todoisToken = runtime.todoist.token

const request = supertest(todoistHost)
// define your endpoints
const requests = {
    getAllProjects: (test) => {
        return request.get('/projects') // set the url and method
            .auth(todoisToken, { type: 'bearer' }) // define your auth type
            .timeout(timeout) // set your timeout
            // retry n times if it fails, wait n seconds between failures, do not retry when code response is 401 or 404
            .retry(3, 5000, [401, 404])
            .then((res) => {
                // use fullLogToReport function to log your request and response
                log.fullLogToReport(test, res, `GET ${todoistHost}/projects`)
                return res
            })
    }
}

module.exports = requests
```

3. Create a js file to save your JSON validation schemas (i.e. schemas/newAPIFolder/projects.schema.js), inside **schemas** folder add a new microservice/API folder in case the folder for you API has not been created yet.

```javascript
const schemas = {
    getAllUpdatedTasks: {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "array",
        "items": [
          {
            "type": "object",
            "properties": {
              "id": {
                "type": "integer"
              },
              "assigner": {
                "type": "integer"
              }
            },
            "required": [
              "id",
              "assigner"
            ]
          }
        ]
      }
      // Add as many json schemas as you need.
}

module.exports = schemas
```

4. If the test folder for you API has not been created yet create a new one into the **tests** folder and then create a new test file (i.e. project.e2e.test.js)

```javascript
// use mocha framework to structure your test
describe('Projects E2E Testing', function () {
    // declare variables that can be use along the tests if needed
    let projectID = null
    // you can use faker to get random test data
    const projectName = faker.lorem.words()
    const colorProject = faker.random.number({ min: 30, max: 49 })
    const favoriteProject = faker.random.boolean()

    it('GET /projects', async function () {
        const res = await projects.getAllProjects(this) // hit the endpoint
        // make some validations
        expect(res.status).to.be.eq(200)
        expect(res.body).to.not.be.empty()
        // use validateSchema function to validate your body response structure
        const schemaResult = await baseTest.validateSchema(this, res.body, schemas.getAllProjects)
        expect(schemaResult.valid, 'Schema validation failed').to.be.true()
    })

    it('POST /projects', async function () {
        const args = { name: 'Movies to not watch' }
        const res = await projects.createNewProject(this, args)
        expect(res.status).to.be.eq(200)
        expect(res.body).to.not.be.empty()
        const schemaResult = await baseTest.validateSchema(this, res.body, schemas.createNewProject)
        expect(schemaResult.valid, 'Schema validation failed').to.be.true()
        projectID = res.body.id // save your key values to use them in other endpoints 
    })

    it('POST /projects/:id', async function () {
        // define your body request
        const args = {
            name: projectName,
            color: colorProject,
            favorite: favoriteProject
        }
        // hit your endpoint with dynamic parameters
        const res = await projects.updateProject(this, projectID, args)
        // make some validations
        expect(res.status).to.be.eq(204)
        expect(res.body).to.be.empty()
    })
})
```

5. Add a new script in the package.json


### ToDo
- inlude the ability to create and run load tests.

**Happy Testing!**
