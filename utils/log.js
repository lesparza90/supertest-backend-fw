const addContext = require('mochawesome/addContext')

const logtoMochawesome = {
    logRequestToReport: function (test, query, bodyRquest = { }) {
        addContext(test, { title: 'Query', value: query })
        addContext(test, { title: 'Body Request:', value: bodyRquest })
    },
    logResponseToReport: function (test, res) {
        addContext(test, { title: 'Status Response:', value: res.status })
        addContext(test, { title: 'Body Response:', value: res.body })
    },
    fullLogToReport: function (test, res, query, bodyRquest = {}) {
        addContext(test, { title: 'Query', value: query })
        addContext(test, { title: 'Body Request:', value: bodyRquest })
        addContext(test, { title: 'Status Response:', value: res.status })
        addContext(test, { title: 'Body Response:', value: res.body })
    }
}

module.exports = logtoMochawesome
