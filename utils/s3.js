const s3FolderUpload = require('s3-folder-upload')
// ES6: import s3FolderUpload from 's3-folder-upload'
 
const directoryName = './tests/todoist/'
// I strongly recommend to save your credentials on a JSON or ENV variables, or command line args
const credentials = {
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: 'us-east-1',
  bucket: 'backend-testing-framework'
}
 
// optional options to be passed as parameter to the method
const options = {
  useFoldersForFileTypes: false,
  useIAMRoleCredentials: false
}
 
// optional cloudfront invalidation rule
const invalidation = {
}

async function uploadFolderToS3 () {
    await s3FolderUpload(directoryName, credentials, options, invalidation)
}
 

module.exports = uploadFolderToS3
