/**
     * Build 'slack.attachments' and save to config
     */
    function buildAttachments(testFileName) {
        const uuid = grunt.config('runtime.uuid')
        const timestamp = grunt.config('runtime.timestamp')
        const daysValid = 30
        const duration = moment.duration({ days: daysValid })
        const ts = moment().add(duration)
        const textLink = grunt.config.get('runtime.signed_url')
        const text = `Service Integration Test Run for ${testFileName}.js`
        const fields = [
            {
                title: 'Timestamp',
                value: timestamp,
                short: true
            },
            {
                title: 'Report Id',
                value: uuid,
                short: true
            },
            {
                title: 'Target Environment',
                value: grunt.config('environment'),
                short: true
            },
            {
                title: 'Test Result Summary',
                value: `${testFileName}.js`,
                short: true
            }
        ];
        const attachment = {
            fallback: 'The test has completed, here are your results.',
            // pretext: pretext,
            author_name: 'Service Integration Tests',
            title: 'Download Test Results',
            title_link: textLink,
            text,
            mrkdwn_in: ['text', 'pretext', 'fields'],
            fields,
            footer: ':aperture: Aperture Science',
            ts: ts.unix()
        }
        return attachment
    }

    function slackAttachments () {
        const body = {
            channel: grunt.config('slack.channel'),
            username: grunt.config('slack.username'),
            icon_emoji: grunt.config('slack.icon_emoji'),
            text: say.random(),
            attachments: buildAttachments()
        }
        request
        .post(grunt.config('slack.webhook_url'))
        .send(JSON.stringify(body))
        .set('content-type', 'application/json')
        .end(function (err, res) {
            if (err) {
                done(false)
                throw err
            }
            done()
        })
    }

    module.exports = slackAttachments()
